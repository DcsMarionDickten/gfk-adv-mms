# Inhalt

- `Protokoll` enthält Word-Datei, die beim Vergleich ausgedruckt vorliegt 
und ausgefüllt und abgezeichnet werden muß
- `source` enthält Rubydateien und ein Unterverzeichnis `data`, in dem die 
Daten-Dateien liegen, die von den Rubydateien bearbeitet werden
- Die meisten Rubydateien enthalten einen Header, der erklärt, was sie tun. 
Aufruf ist immer `> ruby dateiname.rb arg0 arg1 arg2` o.ä. 
- Die meisten Rubydateien sind zur Vorab-Bearbeitung (`remove_line_numbers.rb`,
`switch_columns.rb`etc.)
- Das Shellskript `vorabcheck.sh` nimmt alle Einträge der ersten Spalte beider Dateien,
zählt sie und zählt, wieviele in beiden Dateien gemeinsam sind. Dabei geht
das Skript davon aus, daß eine Datei `p.csv` heißt und die andere `g.csv`
- Das Shellskript `vorabcheck_mit_Params.sh` erlaubt, die beiden Dateien als
Parameter mitzugeben.
- Beide Shellskripte erzeugen Dateien mit irgendwo einem Suffix 2..4, die
Zwischenschritte darstellen.
- `start.sh` macht einen Check auf Duplikate und startet dann `start.rb`. Auch
dies Skript erwartet, daß die Dateien `p.csv` und `g.csv` heißen.

### Die Datei start.rb

(Voraussetzung: Beide Inputdateien haben einen Header mit Spaltenüberschriften
und in der ersten Spalte Hash-Keys. Diese werden als Schlüssel verwendet.)

- Liest gfk-Datei ein (erstes Arg.), bildet einen Hash aus den Einträgen der
ersten Spalte. Zuzüglich einem Eintrag `complete: false`. Hat also die Datei mehrere Zeilen mit demselben HashKey, wird nur
der erste berücksichtigt.
- Liest den Header der zweiten Datei und merkt sich die Spaltenüberschriften
bis auf die erste. Fügt im aufgebauten Hash Einträge mit diesen Spaltenüberschriften
hinzu; Initialwert ist 'NA'.
- Geht die zweite Datei zeilenweise durch. Schaut, ob im aufgebauten Hash
ein Eintrag für einen HashKey (Eintrag erste Spalte) ist. Falls ja, werden
die Einträge der Spalten in die passenden Einträge im aufgebauten Hash kopiert.
Dann wird `complete: true` gesetzt.
- Geht dann den aufgebauten Hash durch und entfernt alle Einträge, wo nicht
`complete:true` steht.
- Generiert dann einen `salt` und hasht den Eintrag der ersten Zeile noch einmal.
- Speichert das Ergebnis in txt-Datei mit Zeitstempel-präfixtem Namen.
- Parameter: In `start.rb` muß der Feldseparator eingestellt werden (`GFK_SPLITTER`,
`MMS_SPLITTER`).
