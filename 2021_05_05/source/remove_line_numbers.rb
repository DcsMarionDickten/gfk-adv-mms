SPLITTER = ","

FILE = 'data/g.csv'

def read_file(filename, max_lines = -1)
  result = []
  line_num=0
  File.open(filename).each do |line|
    print '.'
    result << line
    line_num += 1
    break if (line_num == max_lines)
  end
  print "\n"
  result
end

def start(datei = FILE)
  dateiinhalt = read_file(datei)
  neuer_dateiinhalt = []
  dateiinhalt.each do |zeile|
    zeile_als_array = zeile.split(SPLITTER).collect { |col| col.strip }
    neuezeile = []
    anzahl = zeile_als_array.length
    (1...anzahl).each do |i|# Index 0 einfach weglassen
      neuezeile << zeile_als_array[i]
    end
    neuer_dateiinhalt << neuezeile
  end
  neuer_dateiinhalt
end

def write_output_file(name, data)

  File.delete(name) if File.exist?(name)

  File.open(name, 'a') do |file|

    data.each do |zeile|
      file.write zeile.join(SPLITTER) + "\n"
    end
  end

end


# public static void main()
#
# ------------------------------------------------------------------------------------------------
# sanity checks
# ------------------------------------------------------------------------------------------------

if ARGV.length == 1 && ['--help', '-h'].include?(ARGV[0])
  # output help text
  print "\n"
  print "Dies Programm wirft die Zeilennummer weg,\n"
  print "falls diese in einer CSV-Datei die erste Spalte bildet.\n"
  print "Die Kommandozeilen-Parameter sind:\n"
  print "1. Pfad zur Inputdatei\n"
  print "2. Pfad zur Outputdatei (Vorsicht, wird ohne Nachfrage überschrieben, wenn schon vorhanden)\n"
  print "\n"
  return
elsif ARGV.length < 2
  throw 'not enough arguments: ruby remove_line_numbers.rb input.csv output.csv'
end

# Und los:

inputdatei = ARGV[0]
outputdatei = ARGV[1]

print "input: #{inputdatei}\n"
print "output: #{outputdatei}\n"

throw "File #{inputdatei} not found" unless File.exist?(inputdatei)

write_output_file( outputdatei, start(inputdatei))

