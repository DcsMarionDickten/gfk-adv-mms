# DUPLIKATE IM HASH-KEY WERDEN *NICHT* IN DEN OUTPUT MIT AUFGENOMMEN

require 'set'
require 'digest'

GFK_SPLITTER = "\t"
MMS_SPLITTER = ","

def read_file(filename, max_lines = -1)
  result = []
  line_num=0
  File.open(filename).each do |line|
    result << line
    line_num += 1
    break if (line_num == max_lines)
  end
  result
end

def read_first_line_and_split(file, split_char)
  first_line = read_file(file, 1)[0] || ''
  throw "data file #{file} is empty" if first_line.nil?
  first_line.split(split_char).collect { |col| col.strip }
end

def get_mms_structure(file_mms_data)
  read_first_line_and_split(file_mms_data, MMS_SPLITTER)
end

def get_gfk_structure(file_gfk_data)
  read_first_line_and_split(file_gfk_data, GFK_SPLITTER)
end


def get_gfk_data(data_file_name, structure_data)

  column_count = structure_data.length
  line_num=0
  count_duplicates = 0

  result = {}

  File.open(data_file_name).each do |line|

    line_num += 1
    throw "gfk data - line #{line_num} is empty" if (line || '').strip.length==0

    line.gsub!('#NULL!', ' ')

    values = line.split(GFK_SPLITTER).collect { |val| val.strip }
    throw "gfk data - line #{line_num} has #{values.length} columns, should have been #{column_count}" unless (values.length == column_count)

    key_value = values[0]
    throw "gfk data - line #{line_num} has empty key" if key_value.length==0

    if result[key_value]
      # throw "gfk data - line #{line_num} has duplicate key #{key_value}" if result[key_value]
      count_duplicates += 1
    else

      new_entry = {:complete => false}
      structure_data.each_with_index do |column_name, column_index|
        new_entry[column_name] = values[column_index]
      end

      result[key_value] = new_entry
    end

  end
  print "\n***loaded gfk data -  #{line_num} data records - Found #{count_duplicates} duplicates in gfk data"

  result
end


def add_mms_data(data, mms_structure, mms_data_file)

  column_count = mms_structure.length
  line_num=0
  count_matches = 0
  count_duplicates = 0

  File.open(mms_data_file).each do |line|

    line_num += 1
    throw "MMS data - line #{line_num} is empty" if (line || '').strip.length==0

    next if line_num == 1

    values = line.split(MMS_SPLITTER).collect { |val| val.strip }
    throw "MMS data - line #{line_num} has #{values.length} columns, should have been #{column_count}" unless (values.length == column_count)

    key_value = values[0]
    throw "MMS data - line #{line_num} has empty key" if key_value.length==0

    existing_data = data[key_value]

    if existing_data

      if existing_data[:complete]
        # skipping duplicate
        count_duplicates += 1
      else
        existing_data[:complete] = true
        count_matches += 1
        mms_structure.each_with_index do |column_name, column_index|
          existing_data[column_name] = values[column_index] if column_index > 0 # skip key
        end
      end


    end
  end

  print "*** added MMS data to #{count_matches} of #{line_num-1} data records - Found #{count_duplicates} duplicates in MMS data"
  data

end

def get_key(key)
  key.to_s.rjust(40, '0')
end

def get_random_key
  get_key(rand(0..10000000))
end

def get_new_random_key(key_list)

  key = get_random_key

  while key_list.include?(key)
    # print "dup:#{key}\n"
    key = get_random_key
  end

  key_list << key
  key

end


def count_matches(data)

  result = 0

  data.keys.each do |key|
    record = data[key]
    if record[:complete]
      record.delete(:complete)
      result += 1
    end
  end
  result

end


def remove_unmatched_columns(data)

  result = {}
  key_list = data.keys
  key_list.each do |key|
    record = data[key]
    if record[:complete].nil?
      result.merge!({key => data[key]})
    end
  end
  result

end


def encrypt_key(data)

  result = {}

  #header
  first_record = data[data.keys[0]]
  col_names = first_record.keys
  key_col = col_names[0]

  # generate salt
  salt = get_salt
  print "salt: #{salt}\n"

  data.keys.each do |key|
    record = data[key]
    # overwrite key!
    enc_key = encrypt(key, salt)
    record[key_col] = enc_key
    result[enc_key] = record
  end

  result
end

def write_output_file(name, data)

  File.delete(name) if File.exist?(name)

  File.open(name, 'a') do |file|

    #header
    first_record = data[data.keys[0]]
    col_names = first_record.keys

    print "renaming #{col_names[0]} to Hashcode2\n"

    # rename
    col_names[0] = "Hashcode2"


    file.write col_names.join("\t") + "\n"

    #data
    all_keys = data.keys.sort

    all_keys.each do |key|
      file.write data[key].values.join("\t") + "\n"
    end
  end

end

def get_salt
  rand(36**40).to_s(36)
end

def encrypt(message, salt)
  Digest::SHA256.hexdigest(message + salt)
end

def add_mms_columns(gfk_data, mms_structure)
  result = {}

  gfk_data.keys.each do |key|

    existing_data = gfk_data[key]

    mms_structure.each_with_index do |column_name, column_index|
      existing_data[column_name] = 'NA' if column_index > 0 # skip key
    end

    result[key] = existing_data
  end
  result

end

def get_filename_with_timestamp(filename)
  time = Time.now.to_s.gsub(" ", "_").gsub(":", "").gsub("+0200", "").gsub("+0100", "")
  "./data/#{time}#{filename}"
end


# ------------------------------------------------------------------------------------------------
# sanity checks
# ------------------------------------------------------------------------------------------------

if ARGV.length < 2
  throw 'not enough arguments: ruby start.rb gfk_data.txt mms_data.txt'
end

file_gfk_data = ARGV[0]
file_mms_data = ARGV[1]

print "gfk: #{file_gfk_data}\n"
print "MMS: #{file_mms_data}\n"

throw "gfk data file #{file_gfk_data} not found" unless File.exist?(file_gfk_data)
throw "MMS data file #{file_mms_data} not found" unless File.exist?(file_mms_data)

# ------------------------------------------------------------------------------------------------

start_time = Time.now
print "matching started at #{start_time}\n"

gfk_structure = get_gfk_structure(file_gfk_data)
mms_structure = get_mms_structure(file_mms_data)

gfk_data = get_gfk_data(file_gfk_data, gfk_structure)
gfk_data = add_mms_columns(gfk_data, mms_structure)

print "gfk columns:     #{gfk_structure.length.to_s}\n"
print "gfk rows:        #{gfk_data.length.to_s}\n"
print "MMS columns: #{mms_structure.length.to_s}\n"

all_data = add_mms_data(gfk_data, mms_structure, file_mms_data)

matches = count_matches(all_data)
matched_data = remove_unmatched_columns(all_data)

print "*** matched records: #{matches.to_s}\n"

if matched_data.length > 0
  matched_encrypted_data = encrypt_key(matched_data)
  fname = get_filename_with_timestamp('output.txt')
  write_output_file(fname, matched_encrypted_data)
else
  print "no matching started data found\n"
end

# ------------------------------------------------------------------------------------------------

print "matching stopped at #{Time.now}\n"

print "ACHTUNG: nach den GFK-Headern in der Ergebnisdatei suchen und die überflüssige Zeile entfernen\n"
print "ACHTUNG: wc -l zählt die letzte Zeilen ohne CR nicht mit\n"