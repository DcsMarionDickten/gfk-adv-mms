#!/bin/bash
echo ---------------------
echo --- find duplicates
cat data/g.csv |  awk -F  ";" '{print $1}' | sort | uniq -d > data/g_dups.csv
cat data/p.csv |  awk '{print $1}' | sort | uniq -d > data/p_dups.csv
echo duplicates:
wc -l data/*_dups.csv
echo ---------------------
ruby start.rb ./data/g.csv ./data/p.csv
echo ---------------------
