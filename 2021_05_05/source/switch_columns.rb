# Diese Datei stellt eine Möglichkeit bereit, bei CSV-Dateien, bei denen nicht die richtige Spalte am Anfang steht,
# diese Spalte nach vorne zu stellen. Die Kommandozeilen-Parameter sind:
# 1. Inputdatei
# 2. Outputdatei (Vorsicht, wird ohne Nachfrage überschrieben, wenn schon vorhanden)
# 3. Die Spaltenüberschrift, die nach ganz vorn gestellt werden soll

require 'set'
require 'digest'

SPLITTER = "\t"

FILE = 'data/g.csv'
FIRST_COL_NR = 46

def read_file(filename, max_lines = -1)
  result = []
  line_num=0
  File.open(filename).each do |line|
    result << line
    line_num += 1
    break if (line_num == max_lines)
  end
  result
end

def start(datei, gewuenschte_spalte)
  dateiinhalt = read_file(datei)
  umsortiert = []
  dateiinhalt.each do |zeile|
    zeile_als_array = zeile.split(SPLITTER).collect { |col| col.strip }
    neuezeile = []
    neuezeile[0] = zeile_als_array[gewuenschte_spalte]
    anzahl = zeile_als_array.length
    (0...anzahl).each do |i|
      neuezeile << zeile_als_array[i] unless i == gewuenschte_spalte
    end
    umsortiert << neuezeile
  end
  umsortiert
end

def write_output_file(name, data)

  File.delete(name) if File.exist?(name)

  File.open(name, 'a') do |file|

    data.each do |zeile|
      file.write zeile.join("\t") + "\n"
    end
  end

end

def get_gewuenschte_spalte(datei, g_s)
  dateiinhalt = read_file(datei)
  kopfzeile = dateiinhalt.first.split(SPLITTER).collect { |col| col.strip }
  index = kopfzeile.find_index(g_s)
  throw "Spalte nicht gefunden: #{g_s}" if index == nil?
  index
end

# public static void main()
#
# ------------------------------------------------------------------------------------------------
# sanity checks
# ------------------------------------------------------------------------------------------------

if ARGV.length == 1 && ['--help', '-h'].include?(ARGV[0])
  # output help text
  print "\n"
  print "Diese Datei stellt eine Möglichkeit bereit, bei CSV-Dateien,\n"
  print "bei denen nicht die richtige Spalte am Anfang steht,\n"
  print "diese Spalte nach vorne zu stellen. Die Kommandozeilen-Parameter sind:\n"
  print "1. Pfad zur Inputdatei\n"
  print "2. Pfad zur Outputdatei (Vorsicht, wird ohne Nachfrage überschrieben, wenn schon vorhanden)\n"
  print "3. Die Spaltenüberschrift, die nach ganz vorn gestellt werden soll\n"
  print "\n"
  return
elsif ARGV.length < 3
  throw 'not enough arguments: ruby switch_columns.rb input.csv output.csv gewuenschte_spalte'
end

# OK, here goes...:

inputdatei = ARGV[0]
outputdatei = ARGV[1]
spaltenname = ARGV[2]

print "input: #{inputdatei}\n"
print "output: #{outputdatei}\n"

throw "File #{inputdatei} not found" unless File.exist?(inputdatei)

write_output_file( outputdatei, start(inputdatei, get_gewuenschte_spalte(inputdatei, spaltenname)))

