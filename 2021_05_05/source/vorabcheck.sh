#!/usr/bin/env bash

wc -l data/g.csv
wc -l data/p.csv

cat data/p.csv | awk '{print $1}' > data/p2.txt
sort data/p2.txt > data/p3.txt
uniq data/p3.txt > data/p4.txt
wc -l data/p4.txt

cat data/g.csv | awk '{print $1}' > data/g2.txt
sort data/g2.txt | uniq > data/g4.txt
wc -l data/g4.txt

comm -1 -2 data/g4.txt data/p4.txt > data/diff.txt
wc -l data/diff.txt