#!/usr/bin/env bash
# Zwei Kommaseparierte Dateien. Erste Spalte herausnehmen, Anzahl verschiedener feststellen,
# Anzahl gemeinsamer feststellen. Ausgabe auf Konsole und in Dateien mit Endungen .2, .3, .4.
# Gemeinsame Einträge in erster Spalte sind am Ende in der Datei data/common.txt.
#
# Falls Separator etwas anderes als Komma ist, muß das Argument -F bei awk angepaßt werden
# Falls Separator ein Tabulator ist, kann das Argument -F '...' komplett wegfallen

wc -l $1
wc -l $2
echo ''

cat $1 | awk  '{print $1}' > $1.2
sort $1.2 > $1.3
uniq $1.3 > $1.4
wc -l $1.4
echo ''

cat $2 | awk -F ',' '{print $1}' > $2.2
sort $2.2 > $2.3
uniq $2.3 > $2.4
wc -l $2.4
echo ''

comm -1 -2 $1.4 $2.4 > data/common.txt
wc -l data/common.txt